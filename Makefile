CC = gcc
CFLAGS = -g -std=gnu99
PREFIX=/usr/local

all: xml-pretty-print

ezxml.o: ezxml.c ezxml.h
	$(CC) $(CFLAGS) -c ezxml.c

xml-pretty-print.o: ezxml.h xml-pretty-print.c
	$(CC) $(CFLAGS) -c xml-pretty-print.c

xml-pretty-print: xml-pretty-print.o ezxml.o
	$(CC) $(CFLAGS) xml-pretty-print.o ezxml.o -o xml-pretty-print

clean:
	rm -f *.o xml-pretty-print

install: xml-pretty-print
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -pf xml-pretty-print $(DESTDIR)$(PREFIX)/bin