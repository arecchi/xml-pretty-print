## About
This program is an XML pretty print formatter built upon the [ezxml](http://ezxml.sourceforge.net) library.

It takes an XML file as input and writes the formatted XML to the standard output.

The input to the program can also be provided from the standard input, this being very useful if the program wants to be used as a filter command or as an external formatter for a text editor.

Source code is standard GNU C99 and should be compilable in any POSIX environment.

## Installation
Run ```make``` to compile, then run ```make install``` as root user to install the executable.

Default program location will be ```/usr/local/bin/xml-pretty-print```, a custom destination directory for the executable can be specified by setting variable DESTDIR while building with make.

## Examples
Given the following file ```test.xml```:
```
<note abc="kkf" ss="say hello" ><to>Tove</to><from>Jani</from><heading val="34"><sd reason="cof foekfoe" and="-23123">Reminder</sd></heading><body>Don't forget me this weekend!</body></note>
```
the program can be executed with the file name as input:
```
xml-pretty-print test.xml
```
or streamed from the standard input:
```
xml-pretty-print < test.xml
```
producing on the standard output:
```
<note abc="kkf" ss="say hello">
	<to>Tove</to>
	<from>Jani</from>
	<heading val="34">
		<sd reason="cof foekfoe" and="-23123">Reminder</sd>
	</heading>
	<body>Don't forget me this weekend!</body>
</note>
```

## License
[The MIT License (MIT)](http://opensource.org/licenses/mit-license.php)
